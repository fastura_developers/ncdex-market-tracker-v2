import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AccountPage } from '../account/account';
import { InAppBrowser,InAppBrowserEvent } from '@ionic-native/in-app-browser';
import { timer } from 'rxjs/observable/timer';

/**
 * Generated class for the SubscribePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subscribe',
  templateUrl: 'subscribe.html',
})
export class SubscribePage {

  public surl:string="http://ncdex.fastura.net/ncdex/success.php";
  public furl:string="http://ncdex.fastura.net/ncdex/failure.php";

  constructor(public navCtrl: NavController, public navParams: NavParams, public iab:InAppBrowser) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SubscribePage');
  }
  d(){
    const browser=this.iab.create("http://ncdex.fastura.net/ncdex/ncdexmobile.php","_self",{
      location:'no',
      clearcache:'yes',
      hardwareback:'no',
    });
    browser.on('loadstart').subscribe((event:InAppBrowserEvent)=>{
      if(event.url===this.surl){
       timer(3000).subscribe(()=> browser.close())
        this.navCtrl.push(AccountPage);
      }
      else if(event.url===this.furl){
       timer(3000).subscribe(()=> browser.close())
       this.navCtrl.push(AccountPage);
      }
    });
  }
}
