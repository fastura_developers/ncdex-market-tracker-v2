import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import {Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Push,PushOptions,PushObject } from '@ionic-native/push';
import { NativeGeocoder, NativeGeocoderReverseResult, NativeGeocoderOptions } from '@ionic-native/native-geocoder';
import { DtabsPage } from '../dtabs/dtabs';
import { Geolocation } from '@ionic-native/geolocation';



/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

interface deviceInterface {
  id?: string,
  platform?: string,
  version?: string,
};


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  public details : FormGroup;
  data:any = {};
  public deviceInfo: deviceInterface = {};
  loading: any;
  public lat:number;
  public lng:number;

  constructor(public navCtrl: NavController, public navParams: NavParams,public alertCtrl: AlertController, private device: Device,  public loadingCtrl: LoadingController, public http: Http, private formBuilder: FormBuilder, public nativeGeocoder: NativeGeocoder,public geolocation: Geolocation, private push: Push) {
    this.data.username = '';
    this.data.email = '';
    this.data.phone = '';
    this.data.response = '';
    this.data.addrss = '';
    this.data.id = this.device.uuid;
    this.data.platform = this.device.platform;
    this.data.version = this.device.version;
    this.http = http;

    this.pushSetup();
    
    this.details = this.formBuilder.group({
      username: ['',Validators.compose([
        Validators.pattern('[a-zA-Z0-9]*'),
        Validators.required])],
      email: ['',Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')])],
      phone: ['',Validators.compose([Validators.required])],
    });

    //Get a location
    this.geolocation.getCurrentPosition({enableHighAccuracy: true}).then((resp) => {
      this.lat= resp.coords.latitude;
      this.lng= resp.coords.longitude;
      console.log(this.lat);
      console.log(this.lng);
      this.reverseGeocoding(this.lat,this.lng);
      }).catch((error) => {
         console.log('Error getting location', error);
      });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }
  reverseGeocoding(lat,lng){
    let options: NativeGeocoderOptions = {
      useLocale: false,
      maxResults: 2
    };
    this.nativeGeocoder.reverseGeocode(lat,lng,options)
      .then((result: NativeGeocoderReverseResult[]) =>{ 
        this.data.addrss= result[1].subLocality +","+ result[1].locality +","+
        result[1].subAdministrativeArea +","+ result[1].administrativeArea +"-"+
        result[1].postalCode +","+result[1].countryName;
        console.log(this.data.addrss);
        alert(JSON.stringify(this.data.addrss));
      }).catch((error: any) => console.log(error));
  }

  showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait...',
      duration: 3000
    });
    this.loading.present();
  }

  Submit(){
    this.showLoading();

    var link = 'http://ncdex.fastura.com/ncdex/Register-data.php';
    var myData = JSON.stringify({username: this.data.username, email: this.data.email, phone: this.data.phone, id: this.data.id, platform: this.data.platform, version: this.data.version, add: this.data.addrss });
    this.http.post(link, myData)
    .subscribe(data => {
      this.data.response = data["_body"];
      const alert = this.alertCtrl.create({
        title: 'Register Successfully!',
        subTitle: 'You are logged in',
        buttons: ['OK']
      });
      this.loading.dismiss();
      alert.present();
      this.navCtrl.push(DtabsPage);
    }, error => {
      console.log("Oooops!");
    });
  
    var mail = 'http://ncdex.fastura.net/ncdex/automailncdex.php';
    this.http.post(mail, myData)
    .subscribe(data => {
      this.data.response = data["_body"]; 
    }), error => {
      console.log("Oooops!");
    };
  }
  
  push_user(i) {
    var link = 'http://ncdex.fastura.net/ncdex/Push-data.php';
    var myData = JSON.stringify({id: i});
    this.http.post(link, myData).map(res => res.json())
    .subscribe((data : any) =>
    {
      console.dir("Success");
      console.dir(data);
    }, error => {
     console.log("Oooops!");
    });
  }
  
  pushSetup(){
    const options: PushOptions = {
      android: {
        senderID: '811876770665',
        sound: true,
        vibrate: true,
        icon: 'icon'
      },
      ios: {
        alert: true,
        badge: true,
        sound: true
      }  
    };
     
    const pushObject: PushObject = this.push.init(options);
         
    pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));
     
    pushObject.on('registration').subscribe((registration: any) => {
      console.log('Device registered', registration);
      var i = registration.registrationId;
      console.dir(i);
      this.push_user(i);
    });
     
    pushObject.on('error').subscribe(error => console.error('Error with Push plugin', error));
  }
}