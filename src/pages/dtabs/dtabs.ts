import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { Device } from '@ionic-native/device';
import { ComProvider } from '../../providers/com/com';
import { Http } from '@angular/http';


interface deviceInterface {
  id?: string
};
/**
 * Generated class for the DtabsPage tabs.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dtabs',
  templateUrl: 'dtabs.html'
})
export class DtabsPage {

    public items:Array<any>=[];
  public data:any = {};
  public deviceInfo: deviceInterface = {};
  public myDate: any = new Date().toISOString();

  reportRoot = 'ReportPage';
  callsRoot = 'CallsPage';
  levelsRoot = 'LevelsPage';
  newsRoot = 'NewsPage';
  chartRoot = 'ChartPage';
  

  constructor(public navCtrl: NavController, private device: Device, public http: Http, private com: ComProvider) {
    this.data.id = this.device.uuid;
    this.http = http;
    this.expiredpage();
  }
  expiredpage() {
    var link = 'http://ncdex.fastura.net/ncdex/Subscribe-data.php';
    var myData = JSON.stringify({id: this.data.id});
    this.http.post(link, myData).map(res => res.json())
    .subscribe((data : any) =>
    {
       console.dir(data);
       console.dir(this.myDate);
       this.items = data;
       if(this.items <= this.myDate){
        this.callsRoot = 'SubscribePage';
        this.levelsRoot = 'SubscribePage';
        this.reportRoot = 'ReportPage';
        this.newsRoot = 'NewsPage';
        this.chartRoot = 'ChartPage';
              }
      else{
        this.callsRoot = 'CallsPage';
        this.levelsRoot = 'LevelsPage';
        this.reportRoot = 'ReportPage';
        this.newsRoot = 'NewsPage';
        this.chartRoot = 'ChartPage';
          }
    }, error => {
    console.log("Oooops!");
    });
  }
  shareFB(){
    this.com.shareFacebook();
  }
  sharewap(){
    this.com.shareWhatsapp();
  }
  sharetwit(){
    this.com.shareTwitter();
  }
  moreshare(){
    this.com.moreShare();
  }
}
