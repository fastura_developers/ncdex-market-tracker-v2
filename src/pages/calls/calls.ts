import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';
/**
 * Generated class for the CallsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-calls',
  templateUrl: 'calls.html',
})
export class CallsPage {
  public items:Array<any>=[];

  constructor(public navCtrl: NavController, public navParams: NavParams,public http:HttpClient) {
 this.calls();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CallsPage');
  }
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);
    
    setTimeout(() => {
      console.log('Async operation has ended');
      this.calls();
      refresher.complete();
    }, 1000);
  }
  calls(){
    this.http
    .get('http://ncdex.fastura.net/ncdex/Calls-data.php')
    .subscribe((data : any) =>
    {
       console.dir(data);
       this.items = data;
    }, error => {
    console.log("Oooops!");
    });
  }

}
