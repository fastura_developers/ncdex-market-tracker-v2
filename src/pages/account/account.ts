import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DtabsPage } from '../dtabs/dtabs';

import 'rxjs/add/operator/map';
import { ComProvider } from '../../providers/com/com';
import { Http } from '@angular/http';
import { Device } from '@ionic-native/device';

/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
interface deviceInterface {
  id?: string,
};

@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  public items:Array<any>=[];
  data:any = {};
  public deviceInfo: deviceInterface = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, private device: Device, private com: ComProvider) {
    this.http = http;
    this.data.id = this.device.uuid;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountPage');
    this.details();
  }
  details() {
    var link = 'http://mcx.fastura.net/mcx/Account-data.php';
    var myData = JSON.stringify({id: this.data.id});
    this.http.post(link, myData).map(res => res.json())
    .subscribe((data : any) =>
    {
       console.dir(data);
       this.items = data;
    }, error => {
    console.log("Oooops!");
    });
  }
  TabsPage(){
  	this.navCtrl.push(DtabsPage);
  	this.navCtrl.setRoot(DtabsPage);
  }
  shareFB(){
    this.com.shareFacebook();
  }
  sharewap(){
    this.com.shareWhatsapp();
  }
  sharetwit(){
    this.com.shareTwitter();
  }
  moreshare(){
    this.com.moreShare();
  }
}
