import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, Platform } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';

/*
  Generated class for the ComProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ComProvider {

  public alertShown:boolean=false;
  showSplash = true;
  public appurl : string ='https://play.google.com/store/apps/details?id=com.fastura.ncdex';
  public img : string ='https://lh3.googleusercontent.com/P22IPZKmDKUgrpppgHxWG_z_cbdhwUAFkFmh_VgEjKk2ObaVC1zfWx1ixBKNKxJw6NU=s180-rw';

  constructor(public platform: Platform, public http: HttpClient, private socialSharing: SocialSharing, public alertCtrl: AlertController) {
    console.log('Hello ComProvider Provider');
  }
  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'MCX Market Tracker',
      message: 'Do you want Exit?',
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
            this.alertShown=false;
          }
        },
        {
          text: 'Yes',
          handler: () => {
            console.log('Yes clicked');
            this.platform.exitApp();
          }
        }
      ]
    });
     alert.present().then(()=>{
      this.alertShown=true;
    });
  }

  moreShare(){
    this.socialSharing.share("Ncdex Market Tracker","",this.img,this.appurl).then(()=>{
    console.log("success");
    }).catch(()=>{
    console.error("failed");
    });
  }

  shareTwitter(){
    this.socialSharing.shareViaTwitter("Ncdex Market Tracker",this.img,this.appurl).then(()=>{
    console.log("success");
    }).catch(()=>{
    console.error("failed");
    });
  }

  shareFacebook(){
    this.socialSharing.shareViaFacebook("Ncdex Market Tracker","",this.appurl).then(()=>{
    console.log("success");
    }).catch(()=>{
    console.error("failed");
    });
  }

  shareWhatsapp(){
    this.socialSharing.shareViaWhatsApp("Ncdex Market Tracker",this.img,this.appurl).then(()=>{
    console.log("success");
    }).catch(()=>{
    console.error("failed");
    });
  }

}

