import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SuperTabsModule } from 'ionic2-super-tabs';
import { Device } from '@ionic-native/device';
import { EmailComposer } from '@ionic-native/email-composer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { Push} from '@ionic-native/push';
import { Geolocation } from '@ionic-native/geolocation';
import { NativeGeocoder } from '@ionic-native/native-geocoder';



import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { AboutPage } from '../pages/about/about';
import { DtabsPage } from '../pages/dtabs/dtabs';
import { HttpClientModule} from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ComProvider } from '../providers/com/com';
import { SocialSharing } from '@ionic-native/social-sharing';
import { AccountPage } from '../pages/account/account';
import { HelpPage } from '../pages/help/help';
import { FaqPage } from '../pages/faq/faq';
import { HolidaysPage } from '../pages/holidays/holidays';
import { RatemePage } from '../pages/rateme/rateme';
import { SubscribePage } from '../pages/subscribe/subscribe';
import { NetPage } from '../pages/net/net';
import { RegisterPage } from '../pages/register/register';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    AboutPage,
    DtabsPage,
    AccountPage,
    HolidaysPage,
    FaqPage,
    RatemePage,
    HelpPage,
    SubscribePage,
    NetPage,
    RegisterPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    SuperTabsModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    AboutPage,
    DtabsPage,
    AccountPage,
    HelpPage,
    FaqPage,
    HolidaysPage,
    RatemePage,
    SubscribePage,
    NetPage,
    RegisterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Device,
    SocialSharing,
    EmailComposer,
    Geolocation,
    NativeGeocoder,
    Push,
    Geolocation,
    NativeGeocoder,
    InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ComProvider
  ]
})
export class AppModule {}
