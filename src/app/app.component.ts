import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';



import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { DtabsPage } from '../pages/dtabs/dtabs';
import { AboutPage } from '../pages/about/about';
import { AccountPage } from '../pages/account/account';
import { HelpPage } from '../pages/help/help';
import { FaqPage } from '../pages/faq/faq';
import { RatemePage } from '../pages/rateme/rateme';
import { HolidaysPage } from '../pages/holidays/holidays';
import { timer } from 'rxjs/observable/timer';
import { NetPage } from '../pages/net/net';
import { RegisterPage } from '../pages/register/register';
import { ComProvider } from '../providers/com/com';
import { Device } from '@ionic-native/device';
import { Http } from '@angular/http';


declare var navigator: any;
declare var Connection: any;

interface deviceInterface {
  id?: string
};


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = RegisterPage;
    public deviceInfo: deviceInterface = {};
  data:any = {};
  public items:any = {};
  loading: any;

  pages: Array<{title: string, component: any}>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public com: ComProvider, public device: Device, public http: Http, public loadingCtrl: LoadingController) {
    this.data.id = this.device.uuid;
    this.http = http;

   // this.checkNetwork();

    //Exit Screen display
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      timer(3000).subscribe(()=>this.com.showSplash=false)
       platform.registerBackButtonAction(() => {
       if (this.com.alertShown==false) {
         this.com.presentConfirm();  
        }
      }, 0)
    });
    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', component: DtabsPage },
      { title: 'Account', component: AccountPage },
      { title: 'Holidays', component: HolidaysPage },
      { title: 'FAQ', component: FaqPage },
      { title: 'Rate Me', component: RatemePage },
      { title: 'Help', component: HelpPage },
      { title: 'About Us', component: AboutPage },

    ];

  }


  showLoading(){
    this.loading = this.loadingCtrl.create({
      content: 'Please Wait...'
    });
    this.loading.present();
  }

  /*checkNetwork() {
    this.showLoading();
    this.platform.ready().then(() => {
        var networkState = navigator.connection.type;
        var states = {};
        states[Connection.UNKNOWN]  = 'Unknown connection';
        states[Connection.NONE]     = 'No Network Connection';
        if(states[networkState]=='No Network Connection'){
          this.loading.dismiss();
          this.rootPage=NetPage;
        }else{
          var link = 'http://ncdex.fastura.net/ncdex/Device-data.php';
          var myData = JSON.stringify({id: this.data.id});
          console.dir(myData);
          this.http.post(link, myData).map(res => res.json())
          .subscribe((data : any) =>
          {
            console.dir(data);
            this.items = data;
            if(this.items==this.data.id){
              this.loading.dismiss();
              this.rootPage=DtabsPage;
            }
            else{
              this.loading.dismiss();
              this.rootPage=RegisterPage;
            }
          }, error => {
          console.log("Oooops!");
          });
        }
    });
  }*/
}
